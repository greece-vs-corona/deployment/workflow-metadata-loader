import os
import logging
import requests
# from routines.database import Database
import pymongo


if __name__ == '__main__':

    with pymongo.MongoClient(os.getenv('MONGODB_ENDPOINT')+":"+os.getenv('MONGODB_PORT'),
                             username=os.getenv('MONGODB_USER'),
                             password=os.getenv('MONGODB_PASSWORD'),
                             authSource=os.getenv('MONGODB_AUTHENTICATION_SOURCE')) as mongoclient:
        mongodb = mongoclient[os.getenv('MONGODB_DATABASE')]
        metadata_documents = mongodb["metadata"]

        query = {"finished": 0}
        workflows = metadata_documents.find(query)
        for workflow in workflows:
            cromwell = "http://workflows-" + str(workflow['group']) + "-cromwell" + ":" + str(
                os.getenv('CROMWELL_PORT')) + "/api/workflows/v1"
            r = requests.get(cromwell + "/" +
                             workflow['workflowId'] + "/metadata")
            if not r.ok:
                err = str(r.status_code)
                logging.error(err)
                continue
            ret_json = r.json()
            update_dict = {}
            if "status" in ret_json:
                update_dict['status'] = ret_json['status']
            if "end" in ret_json:
                update_dict['end'] = ret_json['end']
            if "start" in ret_json:
                update_dict['start'] = ret_json['start']
            if "submission" in ret_json:
                update_dict['submission_date_time'] = ret_json['submission']

            if "failures" in ret_json:
                # ret_json['failures'] is an array of dictionaries in the form
                # {message: (type string) , causedBy:(type failures)}
                # that means causedBy is recursively an array of dictionaries of the same form as ret_json['failures']
                # we save it as dictionary field in mongo to preserve the whole structure.
                update_dict['failures'] = ret_json['failures']
            # set to not handle again if succeeded or failed.
            if workflow['status'] == "Failed":
                update_dict['finished'] = 1
            elif workflow['status'] == "Succeeded":
                # get a json with the outputs locations in s3 and save it as a dictionary in Mongo DB to preserve structure.
                r = requests.get(cromwell + "/" +
                                 workflow['workflowId'] + "/outputs")
                if not r.ok:
                    err = str(r.status_code)
                    logging.error(err)
                    continue
                ret_json = r.json()
                update_dict['outputs'] = ret_json['outputs']
                update_dict['finished'] = 1
            query = {"_id": workflow["_id"]}
            new_value = {"$set": update_dict}
            metadata_documents.update_one(query, new_value)
