FROM python:3.6-alpine3.9

COPY requirements.txt /app/

WORKDIR /app

RUN pip install -r requirements.txt

COPY runner.py /app/

ENTRYPOINT [ "python", "/app/runner.py" ]
